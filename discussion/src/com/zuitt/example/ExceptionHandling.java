package com.zuitt.example;

import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        //Exceptions
        //refers to a problem that arises during the execution of a program.
        // it disrupts the normal flow of the program and terminates it abnormally.

        // Exception Handling
        // refers to managing and catching run-time errors in order to safely run your code.
        // "Compile-time" errors are errors that usually happen when you try to compile a program that is syntactically incorrect or has missing package imports.
        //"Run-time" errors, on the other hand, are errors that happen after compilation and during the execution of the program.

        Scanner input = new Scanner(System.in);

        int num = 0;

        System.out.println("Please enter a number:");

        //try - tries to do/execute the statement.
        try {
            num = input.nextInt();
        }
        //catch - catches the error message.
        catch (Exception e) {
            System.out.println("Invalid input. Please enter a number.");
            //prints the throwable error along with other details like the line number and class name where the exception occurred.
            e.printStackTrace();
        }

        // optional block
        //Note that the finally block is particularly useful when dealing with highly sensitive operation, such as:
        // Ensuring safe operation on databases by closing them if they are no longer need.
        // Operations that have unpredictable situations such as if it's possible for users to input problematic information.
        // Any operation that may be susceptible to abuse and malicious acts, such as downloading or saving files.

        finally {
            System.out.println("You have entered: " + num);
        }


//        num = input.nextInt();
        System.out.println("Hello World");

    }
}
