package com.zuitt.example;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;
import static java.lang.System.out;
public class RepetitionControl {
    public static void main(String[] args){
        //[SECTION] Loops
        // are control structures that allow code blocks to be executed multiple time.

        //While Loop
        // allows for repetitive use of code, similar to for-loops, but are usually used for situations where the content to iterate through is indefinite.

        int x = 0;
//      while(x < 10) {
//         System.out.println("Loop number: " + x);
//         x++;
//      }

        //Do-while Loop
        // Similar to while loops. However, do-while loops will always execute at least once - while loops may not execute at all.

//      int y = 10;
//      do{
//         System.out.println("Countdown: " + y);
//         y--;
//      } while(y > 10);

        //For Loop
        //Syntax
        /*
         * for(initialValue; condition; iteration) {
         *        //code block
         * }
         * */

//      for(int i = 0; i < 10; i++) {
//         //int i = 0 -> initial value
//         //i < 10 -> limiting expression/condition
//         // i++ -> increment/decrement
//
//         System.out.println("Current count: " + i);
//      }

        //For Loop with Array/s
//      int[] intArray = {100, 200, 300, 400, 500};
//
//      for(int i = 0; i < intArray.length; i++) {
//         System.out.println(intArray[i]);
//      }

        //For-each loop with Array
        /*
         *  Syntax:
         *     for(dataType itemName : arrayName) {
         *           //code block
         *     }
         *
         * */

      /*String[] namesArray = {"John", "Paul", "George", "Ringo"};
      for(String name : namesArray) {
         System.out.println(name);
      }*/

        //Nested Loops with Multidimensional Arrays
        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        //For Loop
        //outer loop
      /*for (int row = 0; row < 3; row++) {
         //inner loop
         for (int col = 0; col < 3; col++) {
            System.out.println("classroom[" + row + "][" + col + "] = " + classroom[row][col]);
         }
      }*/

        //For-each Loop with ArrayList
        /*
         *     Syntax:
         *        arrayListName.forEach(Consumer<E> -> // code block);
         *
         *     "->" This is called the lambda operator which is used to separate the parameter and implementation/expression.
         * */

        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(25);
        numbers.add(30);
        System.out.println("ArrayList: " + numbers);
        numbers.forEach(num -> System.out.println("ArrayList: " + num));

        //for-each with Hashmaps
        /*
         *     Syntax:
         *        hashMapNames.forEach((key, value) -> // code block);
         * */

        HashMap<String, Integer> grades = new HashMap<String, Integer>() {
            {
                put("English", 90);
                put("Math", 95);
                put("Science", 97);
                put("History", 94);
            }
        };

        grades.forEach((subject, grade) -> System.out.println(subject + " : " + grade + "\n"));

    }
}
