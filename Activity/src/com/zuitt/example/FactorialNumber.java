package com.zuitt.example;
import java.util.Scanner;

import static java.lang.System.out;

public class FactorialNumber {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int num = 0;
        int answer = 1;
        int counter = 1;

        out.print("\t ~~~Factorial~~~ \n\n");
        out.println("Enter an integer whose factorial will be computed: ");
        num = in.nextInt();

        try {
            if(num < 0){
                out.print("Invalid input! Negative numbers cannot be Factored. Please try again!");
            }else {
                while(num >= counter){
                    answer = answer * counter;
                    counter++;
                }
                out.print("The factorial of " + num + " is " + answer + " using While loop \n");
            }
        }catch(Exception e) {
            out.print("Invalid input! Please input an Integer!");
            e.printStackTrace();
        }

        // reset answer to one
        answer = 1;

        try{
            if(num < 0){
                out.print("Invalid input! Negative numbers cannot be Factored. Please try again!");
            }else{
                for (int x = 1; x <= num; x++){
                    answer = answer * x;
                }
                out.print("The factorial of " + num + " is " + answer + " using For loop ");
            }
        }catch(Exception e) {
            out.print("Invalid input! Please input an Integer!");
            e.printStackTrace();
        }


    }
}
